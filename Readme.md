# Inteliheat

First steps towards intelligent heating.

[New -> Shelly + AWS + own sensor data cache (master branch)](https://paucities.com/home-ventilation-automation-revisited/)

[Old -> Shelly + AWS + Sensor.Community (with_sensor_community_api_v)](https://paucities.com/how-i-learned-to-stop-worrying-and-love-shelly/)

[Old -> NodeMCU + Micropython version + Sensor.Community (nodemcu_micropython_v branch)](https://paucities.com/clever-home-heating-sneak-peek/)