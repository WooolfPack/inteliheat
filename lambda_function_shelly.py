import urllib3
from urllib.parse import urlencode
import json
import logging
import boto3
import time

import config


def lambda_handler(event, context):
    """
    AWS Lambda function.
    Running periodically every minute.
    Heating automation.
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    http = urllib3.PoolManager()

    shelly_switch_url = config.shelly_switch_url
    shelly_status_url = config.shelly_status_url
    shelly_timeout = config.shelly_timeout
    auth = config.auth

    def switch_handler(response):
        """
        Handler for Shelly switch relay api call response.
        :param response: response from Shelly api /device/relay/control
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if response_text["isok"] != True:
                logger.error("Shelly API switch - not OK response.")
                raise Exception("Shelly API switch - not OK response.")
        except:
            logger.error("Shelly API switch - malformed response.")
            fallback()
            raise

    def fallback():
        """
        Fallback scenario - turn heating on. In this case controlled by ordinary thermostats.
        """
        try:
            logger.warning("Fallback scenario triggered.")

            # # turn shelly on
            # payload = {}
            # payload["channel"] = "0"
            # payload["turn"] = "on"
            # logger.info("Switch payload: %s" % str(payload))
            # call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)

            vz = "udrzuj"
            sensor_reading = [("VZ", vz)]
        except:
            logger.exception("Fallback exception.")
            raise

    def get_luftdaten_temp(response):
        """
        Handler for Luftdaten api call response.
        :param response: response from Luftdaten api
        :return: temperature
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if len(response_text) != 0:
                temperature = float(response_text[0]["sensordatavalues"][0]["value"])
                logger.info("Luftdaten temperature: %s" % temperature)
                return temperature
            else:
                logger.error("Empty response returned.")
                raise Exception("Empty response returned.")
        except:
            logger.error("Luftdaten API - malformed response.")
            fallback()
            raise

    def call_api(url, timeout, response_handler, auth=None, post=False, fields=None, fallback_flag=False):
        """
        Call api.
        :param url:
        :param timeout:
        :param response_handler:
        :param post: if True, send post request
        :param fields: payload for post request
        :param fallback_flag: if True activate fallback in case of api call fail
        :return: vars returned by handler, retrieved from response
        """
        try:
            if auth is not None:
                url = url + urlencode(auth)
            if not post:
                response = http.request('GET', url, timeout=timeout, retries=False)
            else:
                http.clear()
                response = http.request('POST', url, timeout=timeout, retries=False, fields=fields)
            status_code = response.status
            if status_code == 200:
                handled = response_handler(response)
                return handled
            else:
                logger.error("API call error - non-200 status code returned: %s. Url: %s Detail: %s" %
                             (status_code, url, json.loads(response.data.decode('utf-8'))))
                raise Exception("API call error - non-200 status code returned: %s.  Url: %s" % (status_code, url))
        except:
            logger.exception("API call error - failed calling the endpoint. Url: %s" % url)
            if fallback_flag:
                fallback()
            raise

    def get_shelly_temp(response):
        """
        Handler for Shelly status api call response.
        :param response: response from Shelly api endpoint /device/status
        :return: is on state (bool), building temperature (float), water temperature (float)
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if response_text["isok"] is True:
                print()
                temp_water = float(response_text["data"]["device_status"]["ext_temperature"]["0"]["tC"])
                temp_build = float(response_text["data"]["device_status"]["ext_temperature"]["1"]["tC"])
                ison = response_text["data"]["device_status"]["relays"][0]["ison"]
                logger.info("Shelly temperatures: %s, %s" % (temp_build, temp_water))
                return ison, temp_build, temp_water
            else:
                logger.error("Shelly API status - not OK response.")
                raise Exception("Shelly API status - not OK response.")
        except:
            logger.error("Shelly API status - malformed response.")
            fallback()
            raise

    ison, temp_build, temp_water = call_api(shelly_status_url, shelly_timeout, get_shelly_temp, post=True, auth=auth)

    ssm = boto3.client('ssm')
    res = ssm.get_parameters(Names=['temp_out_divin'])
    temp_out = float(res['Parameters'][0]['Value'])

    ####################################
    # control logics
    temp_on = 66.0 - temp_out - temp_build
    temp_off = temp_on + 5.0

    payload = {}
    payload["channel"] = "0"

    # logger.info("temp_out: %s, temp_in: %s, temp_build: %s, temp_water: %s, temp_on: %s, temp_off: %s" %
    #             (temp_out, temp_in, temp_build, temp_water, temp_on, temp_off))

    if temp_water <= temp_on:
        payload["turn"] = "on"
        logger.info("Turn ON.")
        vz = "zapni"

        # logger.info("Switch payload: %s" % str(payload))
        if not ison:
            # non-ideal, there is 1 second limit between requests for shelly api
            time.sleep(1)
            call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
    elif temp_water > temp_off:
        payload["turn"] = "off"
        logger.info("Turn OFF.")
        vz = "vypni"

        # logger.info("Switch payload: %s" % str(payload))
        if ison:
            # non-ideal, there is 1 second limit between requests for shelly api
            time.sleep(1)
            call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
    else:
        vz = "udrzuj"

    ####################################

    return {
        'statusCode': 200,
        'body': json.dumps('All good!')
    }
